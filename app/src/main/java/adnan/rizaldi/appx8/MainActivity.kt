package adnan.rizaldi.appx8

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    val FONT_SUBTITLE = "subFont"
    val FONT_DETAIL = "F"
    val DEF_FONT_SIZE = 20
    val DEF_TEXT = "Hello World"
    val bgHeader = "header"
    val bgMain = "background"
    val DEF_BACK = "White"
    val TITLE_FILM = "Godzilla vs Kong"
    val DEFF_FILM = "Godzilla vs Kong"
    val CONTENT_FILM = "Content Film"
    val DEFF_CONTENT_FILM = "Godzilla vs. Kong adalah sebuah film monster Amerika Serikat garapan Adam Wingard. Sebuah sekuel dari Godzilla: King of the Monsters dan Kong: Skull Island, film tersebut adalah film keempat dalam seri MonsterVerse yang diproduksi oleh Legendary."

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSet -> {
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun getBackground(myString: String, b:Boolean){
        if(myString.equals("Green")){
            if (b){
                constraintLayout.setBackgroundColor(Color.GREEN)
                txJudul.setTextColor(Color.BLACK)
                txSubtitle.setTextColor(Color.BLACK)
                txDetail.setTextColor(Color.BLACK)
            }else{
                linearLayout.setBackgroundColor(Color.GREEN)
                txHeader.setTextColor(Color.BLACK)
            }
        }
        if(myString.equals("Blue")){
            if (b){
                constraintLayout.setBackgroundColor(Color.BLUE)
                txJudul.setTextColor(Color.WHITE)
                txSubtitle.setTextColor(Color.WHITE)
                txDetail.setTextColor(Color.WHITE)
            }else{
                linearLayout.setBackgroundColor(Color.BLUE)
                txHeader.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Yellow")){
            if (b){
                constraintLayout.setBackgroundColor(Color.YELLOW)
                txJudul.setTextColor(Color.BLACK)
                txSubtitle.setTextColor(Color.BLACK)
                txDetail.setTextColor(Color.BLACK)
            }else{
                linearLayout.setBackgroundColor(Color.YELLOW)
                txHeader.setTextColor(Color.BLACK)
            }
        }
        if(myString.equals("Black")){
            if (b){
                constraintLayout.setBackgroundColor(Color.BLACK)
                txJudul.setTextColor(Color.WHITE)
                txSubtitle.setTextColor(Color.WHITE)
                txDetail.setTextColor(Color.WHITE)
            }else{
                linearLayout.setBackgroundColor(Color.BLACK)
                txHeader.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Red")){
            if (b){
                constraintLayout.setBackgroundColor(Color.RED)
                txJudul.setTextColor(Color.WHITE)
                txSubtitle.setTextColor(Color.WHITE)
                txDetail.setTextColor(Color.WHITE)
            }else{
                linearLayout.setBackgroundColor(Color.RED)
                txHeader.setTextColor(Color.WHITE)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val bgBack = preferences.getString(bgMain,DEF_BACK).toString()
        getBackground(bgBack,true)
        val bgHd = preferences.getString(bgHeader,DEF_BACK).toString()
        getBackground(bgHd,false)
        txJudul.setText(preferences.getString(TITLE_FILM,DEFF_FILM))
        txSubtitle.setTextSize(preferences.getInt(FONT_SUBTITLE,DEF_FONT_SIZE).toFloat())
        txDetail.setText(preferences.getString(CONTENT_FILM,DEFF_CONTENT_FILM))
        txDetail.setTextSize(preferences.getInt(FONT_DETAIL,DEF_FONT_SIZE).toFloat())
        Toast.makeText(this,"Perubahan telah disimpan.",Toast.LENGTH_SHORT).show()
    }






}
