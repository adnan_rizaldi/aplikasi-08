package adnan.rizaldi.appx8

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity: AppCompatActivity(), View.OnClickListener {

    var background: String = ""
    var selectBack : String = ""
    var currentSpin = ""
    var checkRadio = ""
    var checkHeader = ""
    var getTitle = ""

    lateinit var adapterSpin : ArrayAdapter<String>
    val arrayBg = arrayOf("Blue","Yellow","Green","Black","Red")

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    //    spinner
    val bgMain = "background"
    val DEF_BACK = "White"
    val bgHeader = "header"
    val TITLE_FILM = "Godzilla vs Kong"
    val DEFF_FILM = "Godzilla vs Kong"
    val CONTENT_FILM = "Content Film"
    val DEFF_CONTENT_FILM = "Godzilla vs. Kong adalah sebuah film monster Amerika Serikat garapan Adam Wingard. Sebuah sekuel dari Godzilla: King of the Monsters dan Kong: Skull Island, film tersebut adalah film keempat dalam seri MonsterVerse yang diproduksi oleh Legendary. "
    val FONT_SUBTITLE = "subFont"
    val DEF_FONT_SIZE = 10
    val FONT_DETAIL = "F"

    override fun onClick(v: View?) {
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val prefEdit = preferences.edit()
        prefEdit.putString(bgMain,selectBack)
        prefEdit.putString(bgHeader,checkRadio)
        prefEdit.putString(TITLE_FILM,txJudul.text.toString())
        prefEdit.putInt(FONT_SUBTITLE,skSubtitle.progress)
        prefEdit.putString(CONTENT_FILM,txDetail.text.toString())
        prefEdit.putInt(FONT_DETAIL,skDetail.progress)
        prefEdit.commit()
        System.exit(0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        currentSpin = preferences.getString(bgMain,DEF_BACK).toString()
        adapterSpin = ArrayAdapter(this,android.R.layout.simple_list_item_1,arrayBg)
        checkHeader = preferences.getString(bgHeader,DEF_BACK).toString()
        txJudul.setText(preferences.getString(TITLE_FILM,DEFF_FILM))
        txDetail.setText(preferences.getString(CONTENT_FILM,DEFF_CONTENT_FILM))
        skSubtitle.progress = preferences.getInt(FONT_SUBTITLE,DEF_FONT_SIZE)
        skDetail.progress = preferences.getInt(FONT_DETAIL,DEF_FONT_SIZE)
        spBackground.adapter = adapterSpin
        spBackground.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                background = adapterSpin.getItem(position).toString()
                if(background.equals("Blue")){
                    selectBack = "Blue"
                }
                if(background.equals("Yellow")){
                    selectBack = "Yellow"
                }
                if(background.equals("Green")){
                    selectBack = "Green"
                }
                if(background.equals("Black")){
                    selectBack = "Black"
                }
                if(background.equals("Red")){
                    selectBack = "Red"
                }
            }
        }

        radioGroup.setOnCheckedChangeListener{group, checkedId ->
            when(checkedId){
                R.id.rdBlue->{
                    checkRadio = "Blue"
                }R.id.rdYellow->{
                checkRadio = "Yellow"
            }R.id.rdGreen->{
                checkRadio = "Green"
            }R.id.rdBlack->{
                checkRadio = "Black"
            }
            }
        }

        btnSimpan.setOnClickListener(this)
    }



    override fun onStart() {
        super.onStart()
        spBackground.setSelection(getIndex(spBackground,currentSpin))
        getChecked()
    }

    fun getIndex(spinner: Spinner, myString: String) : Int {
        var a = spinner.count
        var b : String =""
        for(i in 0 until a){
            b = arrayBg.get(i)
            if(b.equals(myString,ignoreCase = true)){
                return  i
            }
        }
        return 0
    }

    fun getChecked(){
        if(checkHeader.equals("Blue")){
            rdBlue.isChecked = true
        }
        if(checkHeader.equals("Green")){
            rdGreen.isChecked = true
        }
        if(checkHeader.equals("Yellow")){
            rdYellow.isChecked = true
        }
        if(checkHeader.equals("Black")){
            rdBlack.isChecked = true
        }
    }






}